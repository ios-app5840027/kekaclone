//
//  ForgotViewController.swift
//  kekaClone
//
//  Created by Divyansh on 27/05/24.
//

import UIKit


    import UIKit
    import FirebaseAuth

    class ForgotViewController: UIViewController {

        @IBOutlet weak var emailTextField: UITextField!

        override func viewDidLoad() {
            super.viewDidLoad()
        }

        @IBAction func resetButtonTapped(_ sender: Any) {
            guard let email = emailTextField.text, !email.isEmpty else {
               
                return
            }

            Auth.auth().sendPasswordReset(withEmail: email) { error in
                if let error = error {
                    self.displayError(message: "This email is not registered")
                    print("Error resetting password: \(error.localizedDescription)")
             
                } else { self.displayError(message: "mail have been sented to the email Address")
                   
                    print("Password reset email sent successfully.")
                   
                }
            }
        }
        
        func displayError(message: String) {
          
            let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            
            
        }
            
    }



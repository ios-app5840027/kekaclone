//
//  LeavesTableViewCell.swift
//  kekaClone
//
//  Created by Divyansh on 11/06/24.
//

import UIKit

class LeavesTableViewCell: UITableViewCell {

    @IBOutlet weak var subLabelLeaves: UILabel!
    @IBOutlet weak var mainLabelLeaves: UILabel!
    @IBOutlet weak var imageViewLeaves: UIImageView!
    @IBOutlet weak var noOfLeaves: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  LeaveOptionsVC.swift
//  kekaClone
//
//  Created by Divyansh on 11/06/24.
//

import UIKit
import Firebase

protocol LeaveOptionsVCDelegate: AnyObject {
    func didSelectLeaveType(_ leaveType: String)
}
class LeaveOptionsVC: UIViewController {
    var arr = ["Earned Leaves","Sick Leave", "Casual Leave"]
    var data = ["earned" , "sick" , "casual"]
    var userDataManager = UserDataManager()
    var leaveCounts: [String: Int] = [:]
    @IBOutlet weak var myTableView: UITableView!
    weak var delegate: LeaveOptionsVCDelegate?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchLeaveCounts()
      
    let tableCellNib = UINib(nibName: "LeavesTableViewCell", bundle: nil)
        myTableView.register(tableCellNib, forCellReuseIdentifier: "LeavesTableViewCell")
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func fetchLeaveCounts() {
            userDataManager.initializeLeaveBalance { success in
                if success {
                    
                    self.leaveCounts = self.userDataManager.leaveCounts.compactMapValues({ $0["count"] as? Int })
                    print(self.leaveCounts)
                    DispatchQueue.main.async {
                        self.myTableView.reloadData()
                    }
                } else {
                    print("Failed to fetch leave counts")
                }
            }
        }

   }

extension LeaveOptionsVC : UITableViewDataSource , UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arr.count
        }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = myTableView.dequeueReusableCell(withIdentifier: "LeavesTableViewCell", for: indexPath) as! LeavesTableViewCell
        cell.mainLabelLeaves.text = arr[indexPath.row]
        
         let leaveType = data[indexPath.row]
         

        
         if let count = leaveCounts[leaveType] {
             cell.noOfLeaves.text = "\(count)"
         } else {
             cell.noOfLeaves.text = "0"
         }

         return cell
     }

        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let leaveType = data[indexPath.row]
            delegate?.didSelectLeaveType(leaveType)
            navigationController?.popViewController(animated: true)
        }
    }

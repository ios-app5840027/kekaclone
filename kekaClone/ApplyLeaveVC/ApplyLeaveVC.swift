//
//  ApplyLeaveVC.swift
//  kekaClone
//
//  Created by Divyansh on 10/06/24.
//

import UIKit

class ApplyLeaveVC: UIViewController, UITextViewDelegate , LeaveOptionsVCDelegate{
    
    func didSelectLeaveType(_ leaveType: String) {
           leaveTypeTextField.text = leaveType
       }
    @IBOutlet weak var notifyButton: UIButton!
    @IBOutlet weak var noteTextField: UITextView!
    
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var leaveType: String = "casual"
    let userDataManager = (UIApplication.shared.delegate as? AppDelegate)?.userDataManager
    @IBOutlet weak var leaveTypeTextField: UITextField!
    let placeholderText = "Ex: Need to attend a family function."
    override func viewDidLoad() {
        super.viewDidLoad()
        UI()
        noteTextField.delegate = self
        
        if noteTextField.text.isEmpty {
                    setupInitialTextView()
                }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(leaveTypeTextFieldTapped))
               leaveTypeTextField.addGestureRecognizer(tapGesture)
    }
    
    @objc func leaveTypeTextFieldTapped() {
        let leaveOptionsVC = storyboard?.instantiateViewController(withIdentifier: "LeaveOptionsVC") as! LeaveOptionsVC
        leaveOptionsVC.delegate = self
        navigationController?.pushViewController(leaveOptionsVC, animated: true)
    }
    
    func setupInitialTextView() {
           noteTextField.text = placeholderText
           noteTextField.textColor = UIColor.lightGray
       }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
            if textView.text == placeholderText && textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.black
            }
        }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Ex: Need to attend a family function."
            textView.textColor = UIColor.lightGray
        }
    }
    
  
    
    
    func UI(){
        notifyButton.layer.cornerRadius = notifyButton.frame.size.width / 2
        notifyButton.clipsToBounds = true
        
        noteTextField.layer.borderWidth = 0.5
        noteTextField.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
   
    
    @IBAction func notifyButtonTapped(_ sender: Any) {
    }
    

    @IBAction func requestLeaveTapped(_ sender: Any) {
        guard let leaveType = leaveTypeTextField.text, !leaveType.isEmpty else {
                showAlert(title: "Error", message: "Please select a leave type")
                return
            }
            
            let selectedDate = datePicker.date
            
            guard selectedDate > Date() else {
                showAlert(title: "Error", message: "Please select a valid future date")
                return
            }
            
            let leaveCount = 1
            userDataManager?.deductLeave(type: leaveType, count: leaveCount) { success in
                if success {
                    guard let note = self.noteTextField.text, !note.isEmpty else {
                        self.showAlert(title: "Error", message: "Please enter a note")
                        return
                    }
                    
                    self.userDataManager?.saveLeaveHistory(leaveType: leaveType, note: note, date: selectedDate) { historySaved in
                        if historySaved {
                            self.showAlert(title: "Success", message: "Leave applied successfully") {
                                self.navigationController?.popViewController(animated: true)
                            }
                        } else {
                            self.showAlert(title: "Error", message: "Failed to save leave history")
                        }
                    }
                } else {
                    self.showAlert(title: "Error", message: "Failed to deduct leave")
                }
            }
        }
    
    
    
    func showAlert(title: String, message: String, completion: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            completion?()
        }
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
}



//
//  SideMenuHandler.swift
//  kekaClone
//
//  Created by Divyansh on 21/05/24.
//

import UIKit

class SideMenuHandler: UIViewController {
    
    
    var sideMenuVC: UIViewController?
    var isSideMenuOpen = false
    var overlayView: UIView?
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    
    func openSideMenu() {
        if let sideMenuVC = storyboard?.instantiateViewController(withIdentifier: "SideMenuViewController") as? SideMenuViewController {
            self.sideMenuVC = sideMenuVC

         
            if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
               let window = windowScene.windows.first {
                window.addSubview(sideMenuVC.view)
                sideMenuVC.view.frame = CGRect(x: -260, y: 0, width: 260, height: window.frame.height)

                let overlayView = UIView(frame: window.bounds)
                overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                overlayView.isUserInteractionEnabled = true
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeSideMenu))
                overlayView.addGestureRecognizer(tapGesture)
                window.insertSubview(overlayView, belowSubview: sideMenuVC.view)
                self.overlayView = overlayView

                UIView.animate(withDuration: 0.3, animations: {
                    sideMenuVC.view.frame = CGRect(x: 0, y: 0, width: 260, height: window.frame.height)
                }) { (finished) in
                    self.isSideMenuOpen = true
                }
            }
        }
    }

    @objc func closeSideMenu() {
        guard let sideMenuVC = sideMenuVC else { return }
        if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
           let window = windowScene.windows.first {
            UIView.animate(withDuration: 0.3, animations: {
                sideMenuVC.view.frame = CGRect(x: -260, y: 0, width: 260, height: window.frame.height)
            }) { (finished) in
                sideMenuVC.view.removeFromSuperview()
                self.isSideMenuOpen = false
                self.overlayView?.removeFromSuperview()
                self.overlayView = nil
            }
        }
    }



}

//
//  UserProfileVC.swift
//  kekaClone
//
//  Created by Divyansh on 03/06/24.
//

import UIKit

class UserProfileVC: UIViewController,UINavigationControllerDelegate  {
    
    @IBOutlet weak var mainHeader: UILabel!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profilePhotoImageView: UIImageView!
    
    
    
    override func viewDidLoad() {
          super.viewDidLoad()
        mainHeader.text = NSLocalizedString("profileVC_header", comment: "")

          profilePhotoImageView.layer.cornerRadius = profilePhotoImageView.frame.size.width / 2
          profilePhotoImageView.clipsToBounds = true

          navigationController?.delegate = self
          NotificationCenter.default.addObserver(self, selector: #selector(userDataDidChange), name: UserDataManager.userDataDidChangeNotification, object: nil)

          if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
              let userDataManager = appDelegate.userDataManager

              if let firstName = userDataManager.firstName {
                  updateUI(with: userDataManager)
              } else {
                  userDataManager.fetchUserData { success in
                      if success {
                          DispatchQueue.main.async {
                              self.updateUI(with: userDataManager)
                          }
                      } else {
                          print("Failed to fetch user data")
                      }
                  }
              }
          }
      }

      @objc func userDataDidChange(notification: Notification) {
          DispatchQueue.main.async { [self] in
              if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                  let userDataManager = appDelegate.userDataManager
                  
                  updateUI(with: userDataManager)
          }
         
          }
      }

      func updateUI(with userDataManager: UserDataManager) {
          
          firstNameTextField.text = userDataManager.firstName
          lastNameTextField.text = userDataManager.lastName
          phoneNumberTextField.text = userDataManager.phoneNumber
          emailTextField.text = userDataManager.email
          userNameLabel.text = "\(userDataManager.firstName ?? "") \(userDataManager.lastName ?? "")"
          if let profileImage = userDataManager.profileImage {
              profilePhotoImageView.image = profileImage
          }
      }

      @IBAction func updateButtonDidTap(_ sender: Any) {
          guard let firstName = firstNameTextField.text, !firstName.isEmpty,
                let lastName = lastNameTextField.text, !lastName.isEmpty,
                let phoneNumber = phoneNumberTextField.text, !phoneNumber.isEmpty,
                let email = emailTextField.text, !email.isEmpty else {
              displayError(message: "Please fill in all the information")
              return
          }

          if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
              let userDataManager = appDelegate.userDataManager
              userDataManager.updateUserData(firstName: firstName, lastName: lastName, phoneNumber: phoneNumber, email: email) { success in
                  if success {
                      DispatchQueue.main.async {
                          self.updateUI(with: userDataManager)
                          self.displaySuccess(message: "Profile updated successfully")
                      }
                  } else {
                      self.displayError(message: "Failed to update profile")
                  }
              }
          }
      }

      func displaySuccess(message: String) {
          let alert = UIAlertController(title: "Success", message: message, preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
          present(alert, animated: true, completion: nil)
      }

      func displayError(message: String) {
          let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
          present(alert, animated: true, completion: nil)
      }

      @IBAction func backButtonTapped(_ sender: Any) {
          navigationController?.popViewController(animated: true)
      }

      func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
          if viewController == navigationController.viewControllers.first && navigationController.viewControllers.count == 2 {
              if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene {
                  if let window = windowScene.windows.first, let tabBarController = window.rootViewController as? TabBarController {
                      navigationController.setViewControllers([tabBarController], animated: true)
                  }
              }
          }
      }

      deinit {
          NotificationCenter.default.removeObserver(self, name: UserDataManager.userDataDidChangeNotification, object: nil)
      }
  }

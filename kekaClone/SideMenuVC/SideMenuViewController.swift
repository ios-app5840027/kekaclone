//
//  SideMenuViewController.swift
//  kekaClone
//
//  Created by Divyansh on 20/05/24.
//

import UIKit

class SideMenuViewController: UIViewController {

    @IBOutlet weak var imageProfile: UIImageView!
    
    @IBOutlet weak var languageUiView: UIView!
    
    @IBOutlet weak var LogOutView: UIView!
    
    @IBOutlet weak var userName: UILabel!
    var sideMenuHandler: SideMenuHandler?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageProfile.layer.cornerRadius = imageProfile.frame.width / 2
        imageProfile.clipsToBounds = true
        
        
        let languagesViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(languageViewTapped))
        languageUiView.isUserInteractionEnabled = true
        languageUiView.addGestureRecognizer(languagesViewTapGesture)
        
        
        let logoutTapGesture = UITapGestureRecognizer(target: self, action:#selector(LogoutButtonTapped))
        LogOutView.isUserInteractionEnabled = true
        LogOutView.addGestureRecognizer(logoutTapGesture)
        
        let profilePictureTapGesture = UITapGestureRecognizer(target: self, action:#selector(profilePictureTapped))
        imageProfile.isUserInteractionEnabled = true
        imageProfile.addGestureRecognizer(profilePictureTapGesture)
        
        let userNameTapGesture = UITapGestureRecognizer(target: self, action: #selector(userNameTapped))
        userName.isUserInteractionEnabled = true
        userName.addGestureRecognizer(userNameTapGesture)
       
      
       
        fetchUserDetails()
    }
    
    @objc func languageViewTapped(){
        sideMenuHandler?.closeSideMenu()

      
        guard let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else { return }

       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeViewController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        let navigationController = UINavigationController(rootViewController: homeViewController)
        navigationController.navigationBar.isHidden = true
        sceneDelegate.window?.rootViewController = navigationController

        
      
        let languageViewTapped = storyboard.instantiateViewController(withIdentifier: "LanguagesVC") as! LanguagesVC
        navigationController.pushViewController(languageViewTapped, animated: true)
       
        
    }
    func fetchUserDetails(){
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let userDataManager = appDelegate.userDataManager
            
            if let firstName = userDataManager.firstName {
                updateUI(with: userDataManager)
            } else {
                userDataManager.fetchUserData { success in
                    if success {
                        DispatchQueue.main.async {
                            self.updateUI(with: userDataManager)
                        }
                    } else {
                        print("Failed to fetch user data")
                    }
                }
            }
        }
    }
    
    func updateUI(with userDataManager: UserDataManager) {
    
        userName.text = "\(userDataManager.firstName ?? "") \(userDataManager.lastName ?? "")"
        if let profileImage = userDataManager.profileImage {
            imageProfile.image = profileImage
        }
    }
   
    
    
    
    @objc func userNameTapped(){
        sideMenuHandler?.closeSideMenu()

      
        guard let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else { return }

       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeViewController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        let navigationController = UINavigationController(rootViewController: homeViewController)
        navigationController.navigationBar.isHidden = true
        sceneDelegate.window?.rootViewController = navigationController

        
      
        let userProfileVC = storyboard.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
        navigationController.pushViewController(userProfileVC, animated: true)
       
    }
    
    @objc func profilePictureTapped() {
      
        sideMenuHandler?.closeSideMenu()

      
        guard let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else { return }

       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let TabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        let navigationController = UINavigationController(rootViewController: TabBarController)
        navigationController.navigationBar.isHidden = true
        sceneDelegate.window?.rootViewController = navigationController

        
      
        let userProfileVC = storyboard.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
        navigationController.pushViewController(userProfileVC, animated: true)
    }
    
    
    @objc func LogoutButtonTapped(){
        
       LogOut(message: "Are You Sure")
    }
    func LogOut(message: String) {
      
            let alert = UIAlertController(title: "Want to log out", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.logout()
            }))
            present(alert, animated: true, completion: nil)
        }
    
    
    
    func logout(){
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
            if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                let navController = UINavigationController(rootViewController: loginVC)
                sceneDelegate.window?.rootViewController = navController
            
        }

    }
}


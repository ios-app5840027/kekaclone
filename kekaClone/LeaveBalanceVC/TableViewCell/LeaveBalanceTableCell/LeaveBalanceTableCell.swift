//
//  LeaveBalanceTableCell.swift
//  kekaClone
//
//  Created by Divyansh on 12/06/24.
//

import UIKit

class LeaveBalanceTableCell: UITableViewCell {

    @IBOutlet weak var leaveTypeLabel: UILabel!
    
    @IBOutlet weak var consumedLeaveLabel: UILabel!
    
    @IBOutlet weak var noOfLeaveLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  LeaveHistoryTableCell.swift
//  kekaClone
//
//  Created by Divyansh on 12/06/24.
//

import UIKit

class LeaveHistoryTableCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    
    
    @IBOutlet weak var approvedButton: UIButton!
    @IBOutlet weak var leaveType: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  LeaveBalanceVC.swift
//  kekaClone
//
//  Created by Divyansh on 11/06/24.
//

import UIKit

class LeaveBalanceVC: UIViewController {

    @IBOutlet weak var mainHeadingLabel: UILabel!
    
    @IBOutlet weak var headingCollectionView: UICollectionView!
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    var leaveBalanceData: [String: Int] = [:]
    var headingData = [
            NSLocalizedString("leave_balances", comment: ""),
            NSLocalizedString("leave_history", comment: "")
        ]
    var leaveHistoryData: [LeaveHistoryItem] = []
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchLeaveData()
        myCollectionView.isPagingEnabled = true
        let headingCollectionNib = UINib(nibName: "HeadingCollectionCell", bundle: nil)
        headingCollectionView.register(headingCollectionNib, forCellWithReuseIdentifier: "HeadingCollectionCell")

       let nib = UINib(nibName: "LeaveBalanceCollectionCell", bundle: nil)
        myCollectionView.register(nib, forCellWithReuseIdentifier: "LeaveBalanceCollectionCell")
    }
    
    
    func fetchLeaveData() {
            let userDataManager = (UIApplication.shared.delegate as? AppDelegate)?.userDataManager
            let dispatchGroup = DispatchGroup()
            
            dispatchGroup.enter()
            userDataManager?.fetchLeaveHistory { historyItems in
                self.leaveHistoryData = historyItems
                dispatchGroup.leave()
            }
            
            dispatchGroup.enter()
            userDataManager?.fetchLeaveBalances { leaveBalances in
                self.leaveBalanceData = leaveBalances
                dispatchGroup.leave()
            }
            
            dispatchGroup.notify(queue: .main) {
                self.myCollectionView.reloadData()
            }
        }
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

}

extension LeaveBalanceVC : UICollectionViewDataSource , UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == headingCollectionView {
            return headingData.count
        } else if collectionView == myCollectionView {
            return 2
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            if collectionView == headingCollectionView {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeadingCollectionCell", for: indexPath) as! HeadingCollectionCell
                if selectedIndex == 0 {
                    mainHeadingLabel.text = NSLocalizedString("leave_balances", comment: "")
                }
                else{   
                    mainHeadingLabel.text = NSLocalizedString("leave_history", comment: "")
                }
                cell.headingLabel.text = headingData[indexPath.row]
              
                cell.headingLabel.textColor = selectedIndex == indexPath.row ? .blue.withAlphaComponent(0.6) : .black
                cell.underlineView.backgroundColor = selectedIndex == indexPath.row ? .blue.withAlphaComponent(0.6) : .clear
              
    
                return cell
            } else if collectionView == myCollectionView {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeaveBalanceCollectionCell", for: indexPath) as! LeaveBalanceCollectionCell
                if indexPath.row == 0 {
                    
                    cell.setLeaveBalanceData(data: leaveBalanceData)
                } else if indexPath.row == 1 {
                    cell.setLeaveHistoryData(data: leaveHistoryData)
                }
                return cell
            }
            return UICollectionViewCell()
        }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if collectionView == headingCollectionView{
            
            let width = headingCollectionView.frame.width / 2 - 10
            return CGSize(width: width , height: 50 )
        }
        else
        {
            let height = myCollectionView.frame.height
            let width = myCollectionView.frame.width / 1
            return CGSize(width: width, height: height)
        }
        
    }


    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let xPoint = scrollView.contentOffset.x + scrollView.frame.width / 2
        let yPoint = scrollView.frame.height / 2
        let center = CGPoint(x: xPoint, y: yPoint)
        if let ip = myCollectionView.indexPathForItem(at:center) {
            self.selectedIndex = ip.row
            headingCollectionView.reloadData()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == headingCollectionView {
                  selectedIndex = indexPath.row
                  myCollectionView.scrollToItem(at: IndexPath(item: indexPath.row, section: 0), at: .centeredHorizontally, animated: true)
                  headingCollectionView.reloadData()
              }
    }
    
    
    
    
}

//
//  LeaveBalanceCollectionCell.swift
//  kekaClone
//
//  Created by Divyansh on 11/06/24.
//

import UIKit
enum TableDataType {
    case leaveBalance
    case leaveHistory
}
class LeaveBalanceCollectionCell: UICollectionViewCell {
    
        var dataType: TableDataType = .leaveBalance
    var leaveBalanceData: [String: Any] = [:]
    var leaveHistoryData: [LeaveHistoryItem] = []
      
    @IBOutlet weak var myTableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

     
        self.myTableView.dataSource = self
        self.myTableView.delegate = self
        
        let leavBalanceTableNib = UINib(nibName: "LeaveBalanceTableCell", bundle: nil)
        myTableView.register(leavBalanceTableNib, forCellReuseIdentifier: "LeaveBalanceTableCell")
        
        let leaveHistoryTableNib = UINib(nibName: "LeaveHistoryTableCell", bundle: nil)
        myTableView.register(leaveHistoryTableNib, forCellReuseIdentifier: "LeaveHistoryTableCell")
    }
    func setLeaveBalanceData(data: [String: Int]) {
            self.leaveBalanceData = data
            self.dataType = .leaveBalance
            myTableView.reloadData()
        }
    func setLeaveHistoryData(data: [LeaveHistoryItem]) {
            self.leaveHistoryData = data
            self.dataType = .leaveHistory
            myTableView.reloadData()
        }
}


extension LeaveBalanceCollectionCell : UITableViewDelegate , UITableViewDataSource {
 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch dataType {
        case .leaveBalance:
            return leaveBalanceData.count
           
        case .leaveHistory:
            return leaveHistoryData.isEmpty ? 1 : leaveHistoryData.count
           
        }
    }
           
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch dataType {
        case .leaveBalance:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeaveBalanceTableCell", for: indexPath) as! LeaveBalanceTableCell
            let leaveType = Array(leaveBalanceData.keys)[indexPath.row]
            let leaveCount = leaveBalanceData[leaveType] ?? 0
            cell.leaveTypeLabel.text = "\(leaveType)"
            cell.noOfLeaveLabel.text = "\(leaveCount)"
            return cell
        case .leaveHistory:
                 if leaveHistoryData.isEmpty {
                     let cell = UITableViewCell(style: .default, reuseIdentifier: "NoHistoryCell")
                     cell.textLabel?.text = "No Leave is applied"
                     cell.textLabel?.textAlignment = .center
                     cell.textLabel?.textColor = .gray
                     return cell
                 } else {
                     let cell = tableView.dequeueReusableCell(withIdentifier: "LeaveHistoryTableCell", for: indexPath) as! LeaveHistoryTableCell
                     let historyItem = leaveHistoryData[indexPath.row]
                     cell.dateLabel.text = formatDate(historyItem.date)
                     cell.leaveType.text = "Note: \(historyItem.note)"
                     return cell
                 }
             }
         }
       
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            switch dataType {
            case .leaveBalance:
                return 55
            case .leaveHistory:
                return 55
            }
        }
    
    private func formatDate(_ date: Date) -> String {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMM yyyy"
            return formatter.string(from: date)
        }
}
    


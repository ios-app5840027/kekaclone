//
//  HeadingCollectionCell.swift
//  kekaClone
//
//  Created by Divyansh on 12/06/24.
//

import UIKit

class HeadingCollectionCell: UICollectionViewCell {

    @IBOutlet weak var headingLabel: UILabel!
    
    @IBOutlet weak var underlineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

//
//  SignUpViewController.swift
//  kekaClone
//
//  Created by Divyansh on 27/05/24.
//

import UIKit
import Firebase
import FirebaseStorage

class SignUpViewController: UIViewController {

    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var imageSelectorButton: UIButton!
    
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var imageSelector: UIImageView!
    
    @IBOutlet weak var imageSelectorOptionView: UIView!

    
    var selectedImage: UIImage?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageSelector.layer.cornerRadius = imageSelector.frame.size.width / 2
        imageSelector.layer.borderWidth = 2
        imageSelector.layer.borderColor = UIColor.lightGray.cgColor
        imageSelectorOptionView.isHidden = true
        tapGesture()
    }
    
    func tapGesture(){
        let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageSelectorTapped))
        imageSelector.isUserInteractionEnabled = true
        imageSelector.addGestureRecognizer(imageTapGesture)
        
        let outsideTapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
               outsideTapGesture.cancelsTouchesInView = false
               view.addGestureRecognizer(outsideTapGesture)
    }
    @objc func viewTapped(){
        imageSelectorOptionView.isHidden = true
    }
    
    @objc func imageSelectorTapped(){
        
        imageSelectorOptionView.isHidden = false
        
        
    }
    
    @IBAction func takePhotoButton(_ sender: Any) {
    }
    
    
    
    @IBAction func uploadPhotoFromGalleryButton(_ sender: Any) {
        presentImagePicker(sourceType: .photoLibrary)
        
    }
    
    @IBAction func SignUpButtonTapped(_ sender: Any) {
    
            let loader = showLoader()

            guard let email = emailTextField.text, !email.isEmpty,
                  let firstName = firstNameTextField.text, !firstName.isEmpty,
                  let lastName = lastNameTextField.text, !lastName.isEmpty,
                  let phoneNumber = phoneNumberTextField.text, !phoneNumber.isEmpty,
                  let password = passwordTextField.text, !password.isEmpty,
                  let confirmPassword = confirmPasswordTextField.text, !confirmPassword.isEmpty,
                  let selectedImage = selectedImage else {
                displayError(message: "Please fill in all the information")
                loader.stopAnimating()
                return
            }

            guard password.count >= 6 else {
                displayError(message: "Password must be at least 6 characters long")
                loader.stopAnimating()
                return
            }

            guard password == confirmPassword else {
                displayError(message: "Passwords do not match")
                loader.stopAnimating()
                return
            }

           
            Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                if let error = error {
                    self.displayError(message: error.localizedDescription)
                    loader.stopAnimating()
                    return
                }

                guard let userUID = authResult?.user.uid else {
                    self.displayError(message: "Failed to create user")
                    loader.stopAnimating()
                    return
                }

                self.uploadProfileImage(selectedImage: selectedImage, userUID: userUID) { imageUrl in
                    guard let imageUrl = imageUrl else {
                        self.displayError(message: "Failed to upload profile image")
                        loader.stopAnimating()
                        return
                    }

                    self.saveUserProfile(email: email, firstName: firstName, lastName: lastName, phoneNumber: phoneNumber, profileImageUrl: imageUrl, userUID: userUID) { success in
                        loader.stopAnimating()
                        if success {
                            self.registerSuccess(message: "You are registered successfully")
                        } else {
                            self.displayError(message: "Failed to save user profile")
                        }
                    }
                }
            }
        }

        
        func uploadProfileImage(selectedImage: UIImage, userUID: String, completion: @escaping(_ url: URL?) -> Void) {
            let storageRef = Storage.storage().reference().child("profile_images").child("\(userUID).jpg")

            guard let imageData = selectedImage.jpegData(compressionQuality: 1.0) else {
                print("Failed to convert selected image to JPEG data")
                completion(nil)
                return
            }

            storageRef.putData(imageData, metadata: nil) { metadata, error in
                if let error = error {
                    print("Failed to upload profile image: \(error.localizedDescription)")
                    completion(nil)
                    return
                }

                storageRef.downloadURL { url, error in
                    if let url = url {
                        completion(url)
                    } else {
                        print("Failed to get download URL for uploaded image")
                        completion(nil)
                    }
                }
            }
        }

        
        func saveUserProfile(email: String, firstName: String, lastName: String, phoneNumber: String, profileImageUrl: URL, userUID: String, completion: @escaping((Bool) -> Void)) {
            let databaseRef = Database.database().reference().child("Users").child(userUID)
            
            let initialLeaveCounts: [String: [String: Any]] = [
                    "casual": ["id": "leaveType1", "count": 3],
                    "sick": ["id": "leaveType2", "count": 5],
                    "earned": ["id": "leaveType3", "count": 4]
                ]
            let userObject: [String: Any] = [
                "email": email,
                "firstName": firstName,
                "lastName": lastName,
                "phoneNumber": phoneNumber,
                "profileUrl": profileImageUrl.absoluteString,
                "leaveCounts": initialLeaveCounts
            ]

            databaseRef.setValue(userObject) { error, ref in
                if let error = error {
                    print("Failed to save user profile: \(error.localizedDescription)")
                    completion(false)
                } else {
                    completion(true)
                }
            }
        }

    func pushTologin(){
        navigationController?.popViewController(animated: true)
    }
    
    
    func registerSuccess(message: String) {
      
            let alert = UIAlertController(title: "Success", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.pushTologin()
            }))
            present(alert, animated: true, completion: nil)
        }
        
    
    func displayError(message: String) {
      
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
        
        
    }
    @IBAction func goToLoginButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated:true)
    }
    
}

extension SignUpViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func presentImagePicker(sourceType: UIImagePickerController.SourceType){
         
        if UIImagePickerController.isSourceTypeAvailable(sourceType)
        {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = sourceType
            present(imagePicker,animated: true,completion: nil)}
        
        else
            {
                displayError(message: "Source type not available.")
            }
        }
        
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let selectedImage = info[.editedImage] as? UIImage {
                    imageSelector.image = selectedImage
                    self.selectedImage = selectedImage
                } else if let selectedImage = info[.originalImage] as? UIImage {
                    imageSelector.image = selectedImage
                    self.selectedImage = selectedImage
                }
                
                dismiss(animated: true, completion: nil)
            }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true,completion: nil)
    }
 }
    
extension SignUpViewController {
    
    func uploadProfileImage(completion: @escaping(_ url: URL?) -> Void) {
        guard let uid = Auth.auth().currentUser?.uid else {
            print("Failed to get user UID")
            completion(nil)
            return
        }

        guard let selectedImage = selectedImage else {
            print("Selected image is nil")
            completion(nil)
            return
        }

        let storageRef = Storage.storage().reference().child("profile_images").child("\(uid).jpg")

        guard let imageData = selectedImage.jpegData(compressionQuality: 0.3) else {
            print("Failed to convert selected image to JPEG data")
            completion(nil)
            return
        }

        storageRef.putData(imageData, metadata: nil) { metadata, error in
            if let error = error {
                print("Failed to upload profile image: \(error.localizedDescription)")
                completion(nil)
                return
            }

            storageRef.downloadURL { url, error in
                if let url = url {
                    completion(url)
                } else {
                    print("Failed to get download URL for uploaded image")
                    completion(nil)
                }
            }
        }
    }

    func saveUserProfile(email: String, firstName: String, lastName: String, phoneNumber: String, profileImageUrl: URL, completion: @escaping((Bool) -> Void)) {
        guard let uid = Auth.auth().currentUser?.uid else {
            print("Failed to get user UID")
            completion(false)
            return
        }

        let databaseRef = Database.database().reference().child("Users").child(uid)

        let userObject: [String: Any] = [
            "email": email,
            "firstName": firstName,
            "lastName": lastName,
            "phoneNumber": phoneNumber,
            "profileUrl": profileImageUrl.absoluteString
        ]

        databaseRef.setValue(userObject) { error, ref in
            if let error = error {
                print("Failed to save user profile: \(error.localizedDescription)")
                completion(false)
            } else {
                completion(true)
            }
        }
    }
    
    
    func showLoader() -> UIActivityIndicatorView {
        let loader = UIActivityIndicatorView()
        loader.style = .medium
        loader.center = view.center
        loader.startAnimating()
        view.addSubview(loader)
        return loader
    }

    
}


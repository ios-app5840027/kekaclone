//
//  LoginViewController.swift
//  kekaClone
//
//  Created by Divyansh on 02/05/24.
//

import UIKit
import Firebase
class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var secondButtonView: UIView!
    @IBOutlet weak var buttonView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonView.layer.borderWidth = 0.3
        buttonView.layer.cornerRadius = 3
        buttonView.layer.borderColor = UIColor.darkGray.cgColor
        secondButtonView.layer.borderWidth = 0.3
        secondButtonView.layer.borderColor = UIColor.darkGray.cgColor
        secondButtonView.layer.cornerRadius = 3
    }
    
    @IBAction func continueButtonTapped(_ sender: Any) {
      
        guard let  getEmail = emailTextField.text else { return }
        guard let  getPassword = passwordTextField.text else { return }

        
        Auth.auth().signIn(withEmail: getEmail, password: getPassword) { firebaseResult, error in
            if let e = error {
                
                
                self.displayError(message: "Invalid credentials. Please check your email and password.")
            }
                
           
            else
            { UserDefaults.standard.set(true, forKey: "isLoggedIn")
               
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
     
                let tabBarVC = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
      
                guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
                      let sceneDelegate = windowScene.delegate as? SceneDelegate else {
                    return
                }
           
                sceneDelegate.window?.rootViewController = tabBarVC
            }
        }
  }
        
     
    @IBAction func forgotPasswordTapped(_ sender: Any) {
        let forGotpassword = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgotViewController") as! ForgotViewController
        navigationController?.pushViewController(forGotpassword, animated: true)
        
        
    }
    
    
    @IBAction func goToSignUpButtonTapped(_ sender: Any) {
        let signUpPage = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        navigationController?.pushViewController(signUpPage, animated: true)
        
    }
    
    
    func displayError(message: String) {
      
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
        
        
    }
}

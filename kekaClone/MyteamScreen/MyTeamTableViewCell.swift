//
//  MyTeamTableViewCell.swift
//  kekaClone
//
//  Created by Divyansh on 06/05/24.
//

import UIKit

class MyTeamTableViewCell: UITableViewCell {
    
    enum MyTeamCollection{
        case offweek
        case mydepartment
        case myManager
        case myPeer
    }

    @IBOutlet weak var headinglabel: UILabel!
    
    @IBOutlet weak var labelHeight: NSLayoutConstraint!
    @IBOutlet weak var myTeamCollectionView: UICollectionView!
    
    var currentCollection :MyTeamCollection = .offweek
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.myTeamCollectionView.dataSource = self
        self.myTeamCollectionView.delegate = self
        
        
        let offThisWeekCollectionCellNib = UINib(nibName: "OffWeekCollectionViewCell", bundle: nil)
        myTeamCollectionView.register(offThisWeekCollectionCellNib, forCellWithReuseIdentifier: "OffWeekCollectionViewCell")
        
        let myDepartmentCellNib = UINib(nibName: "MyDepartmentCollectionViewCell", bundle: nil)
        myTeamCollectionView.register(myDepartmentCellNib, forCellWithReuseIdentifier: "MyDepartmentCollectionViewCell")
        
        let TeammatesCellNib = UINib(nibName: "TeammatesCollectionViewCell", bundle: nil)
        myTeamCollectionView.register(TeammatesCellNib, forCellWithReuseIdentifier: "TeammatesCollectionViewCell")
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
     

        
    }
    
}

extension MyTeamTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch currentCollection{
        case .offweek:
            return 1
        case.mydepartment:
            return 1
        case .myManager:
            return 1
        case .myPeer:
            return 4
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch currentCollection{
        case .offweek:
            headinglabel.text = NSLocalizedString("off_this_week_heading", comment: "")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OffWeekCollectionViewCell", for: indexPath) as! OffWeekCollectionViewCell
            return cell
            
        case .mydepartment:
            labelHeight.constant = 0
            headinglabel.isHidden = true
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyDepartmentCollectionViewCell", for: indexPath) as! MyDepartmentCollectionViewCell
            return cell
                
    case .myManager:
        headinglabel.text = NSLocalizedString("manager", comment: "")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TeammatesCollectionViewCell", for: indexPath) as! TeammatesCollectionViewCell
       
      
        return cell
            
            
        case .myPeer:
            headinglabel.text = NSLocalizedString("peers", comment: "")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TeammatesCollectionViewCell", for: indexPath) as! TeammatesCollectionViewCell
           
           
            return cell
        }
    }
        

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch currentCollection{
        case .offweek :
            let width = myTeamCollectionView.frame.width / 1 - 10
            return CGSize(width: width, height: 100)
        case .mydepartment:
            let width = myTeamCollectionView.frame.width / 1 - 20
            
            return CGSize(width: width, height: 90)
       
        case .myManager:
                let width = myTeamCollectionView.frame.width / 1
                return CGSize(width: width, height: 80)
        case .myPeer:
            let width = myTeamCollectionView.frame.width / 1 
            return CGSize(width: width, height: 80)
    }
        
    }
}

//
//  MyTeamViewController.swift
//  kekaClone
//
//  Created by Divyansh on 02/05/24.
//

import UIKit

class MyTeamViewController: SideMenuHandler {

    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var myTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        profilePhoto.layer.cornerRadius = profilePhoto.frame.size.width / 2
        profilePhoto.clipsToBounds = true
        
        let myTeamTableCellNib = UINib(nibName: "MyTeamTableViewCell", bundle: nil)
        myTableView.register(myTeamTableCellNib, forCellReuseIdentifier: "MyTeamTableViewCell")
        
        let searchBarTappGesture = UITapGestureRecognizer(target: self, action: #selector(searchBarDidTapped))
        searchBar.isUserInteractionEnabled = true
        searchBar.addGestureRecognizer(searchBarTappGesture)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(profilePhotoTapped))
        profilePhoto.isUserInteractionEnabled = true
        profilePhoto.addGestureRecognizer(tapGestureRecognizer)
        fetchUserDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            tabBarController?.tabBar.isHidden = true
        }
    
    
    @objc func profilePhotoTapped() {
        
        if isSideMenuOpen  {
            closeSideMenu()
        } else {
            openSideMenu()
        }
    }
    func fetchUserDetails(){
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let userDataManager = appDelegate.userDataManager
            
            if let firstName = userDataManager.firstName {
                updateUI(with: userDataManager)
            } else {
                userDataManager.fetchUserData { success in
                    if success {
                        DispatchQueue.main.async {
                            self.updateUI(with: userDataManager)
                        }
                    } else {
                        print("Failed to fetch user data")
                    }
                }
            }
        }
    }
    
    func updateUI(with userDataManager: UserDataManager) {
        
            if let profileImage = userDataManager.profileImage {
                profilePhoto.image = profileImage
            }
        }
    
    @objc func searchBarDidTapped(){
        let pushToEmployeeDirectory = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EmployeeDirectoryVC") as! EmployeeDirectoryVC
        navigationController?.pushViewController(pushToEmployeeDirectory, animated: true)
    }
    
}


extension MyTeamViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTableView.dequeueReusableCell(withIdentifier: "MyTeamTableViewCell", for: indexPath) as! MyTeamTableViewCell
        switch indexPath.row{
        case 0:
            cell.currentCollection = .offweek
        case 1:
            cell.currentCollection = .mydepartment
        case 2:
            cell.currentCollection = .myManager
            
        default:
            cell.currentCollection = .myPeer
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row{
        case 0:
            return 150
        case 1:
            return 130
        case 2:
            return 130
        default:
            return 400
        }
    }
}

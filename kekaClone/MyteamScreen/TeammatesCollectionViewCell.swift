//
//  TeammatesCollectionViewCell.swift
//  kekaClone
//
//  Created by Divyansh on 06/05/24.
//

import UIKit

class TeammatesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mainImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mainImage.layer.cornerRadius = mainImage.frame.width / 2
        mainImage.clipsToBounds = true
    }

}

//
//  MyDepartmentCollectionViewCell.swift
//  kekaClone
//
//  Created by Divyansh on 06/05/24.
//

import UIKit

class MyDepartmentCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainView.layer.borderWidth = 0.5
        mainView.layer.cornerRadius = 8
        mainView.layer.borderColor = UIColor.purple.cgColor
    }

}

//
//  ViewController.swift
//  kekaClone
//
//  Created by Divyansh on 02/05/24.
//

import UIKit

class InitialScreenVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
    }

    @IBAction func loginButtonTapped(_ sender: Any) {
        let loginViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        navigationController?.pushViewController(loginViewController, animated: true)
    }
    
}




import Firebase

struct LeaveHistoryItem {
    let leaveType: String
    let note: String
    let date: Date
}

class UserDataManager {
    var firstName: String?
    var lastName: String?
    var phoneNumber: String?
    var email: String?
    var profileImage: UIImage?
    var leaveCounts: [String: [String: Any]] = [:]
    static let userDataDidChangeNotification = Notification.Name("UserDataDidChange")

    func fetchUserData(completion: @escaping (Bool) -> Void) {
        guard let uid = Auth.auth().currentUser?.uid else {
            print("Failed to get user UID")
            completion(false)
            return
        }

        let databaseRef = Database.database().reference().child("Users").child(uid)

        databaseRef.observeSingleEvent(of: .value) { snapshot in
            
            guard let userData = snapshot.value as? [String: Any] else {
                print("Failed to fetch user data")
                completion(false)
                return
            }

            self.firstName = userData["firstName"] as? String
            self.lastName = userData["lastName"] as? String
            self.phoneNumber = userData["phoneNumber"] as? String
            self.email = userData["email"] as? String
            self.leaveCounts = userData["leaveCounts"] as? [String: [String: Any]] ?? [:]

            
            if let profileUrlString = userData["profileUrl"] as? String, let profileUrl = URL(string: profileUrlString) {
                URLSession.shared.dataTask(with: profileUrl) { data, response, error in
                    if let data = data, let image = UIImage(data: data) {
                        self.profileImage = image
                    }
                    
                    NotificationCenter.default.post(name: UserDataManager.userDataDidChangeNotification, object: nil)
                    completion(true)
                }.resume()
            } else {
                NotificationCenter.default.post(name: UserDataManager.userDataDidChangeNotification, object: nil)
                completion(true)
            }
        }
    }

    func updateUserData(firstName: String?, lastName: String?, phoneNumber: String?, email: String?, completion: @escaping (Bool) -> Void) {
        guard let uid = Auth.auth().currentUser?.uid else {
            print("Failed to get user UID")
            completion(false)
            return
        }

        let databaseRef = Database.database().reference().child("Users").child(uid)

        var updates: [String: Any] = [:]
        if let firstName = firstName {
            updates["firstName"] = firstName
        }
        if let lastName = lastName {
            updates["lastName"] = lastName
        }
        if let phoneNumber = phoneNumber {
            updates["phoneNumber"] = phoneNumber
        }
        if let email = email {
            updates["email"] = email
        }

        databaseRef.updateChildValues(updates) { error, ref in
            if let error = error {
                print("Failed to update user data: \(error.localizedDescription)")
                completion(false)
            } else {
                self.firstName = firstName
                self.lastName = lastName
                self.phoneNumber = phoneNumber
                self.email = email

                NotificationCenter.default.post(name: UserDataManager.userDataDidChangeNotification, object: nil)
                completion(true)
            }
        }
    }
    
    func initializeLeaveBalance(completion: @escaping (Bool) -> Void) {
        guard let uid = Auth.auth().currentUser?.uid else {
            print("Failed to get user UID")
            completion(false)
            return
        }

        let databaseRef = Database.database().reference().child("Users").child(uid).child("leaveCounts")
        
        databaseRef.observeSingleEvent(of: .value) { snapshot in
            guard let leaveCountsData = snapshot.value as? [String: [String: Any]] else {
                print("Failed to fetch leave counts")
                completion(false)
                return
            }
            
        
            self.leaveCounts = [:]
            
            for (leaveType, leaveData) in leaveCountsData {
               
                guard let count = leaveData["count"] as? Int,
                      let leaveID = leaveData["id"] as? String else {
                    continue
                }
                
               
                switch leaveID {
                case "leaveType1":
                    self.leaveCounts["casual"] = ["count": count]
                case "leaveType2":
                    self.leaveCounts["sick"] = ["count": count]
                case "leaveType3":
                    self.leaveCounts["earned"] = ["count": count]
                default:
                    break
                }
            }
            
            NotificationCenter.default.post(name: UserDataManager.userDataDidChangeNotification, object: nil)
            completion(true)
        }
    }

    func saveLeaveHistory(leaveType: String, note: String, date: Date, completion: @escaping (Bool) -> Void) {
       guard let uid = Auth.auth().currentUser?.uid else {
           print("Failed to get user UID")
           completion(false)
           return
       }

       let historyRef = Database.database().reference().child("Users").child(uid).child("leaveHistory").childByAutoId()

       let leaveHistory: [String: Any] = [
           "leaveType": leaveType,
           "note": note,
           "date": date.timeIntervalSince1970
       ]

       historyRef.setValue(leaveHistory) { error, _ in
           if let error = error {
               print("Failed to save leave history: \(error.localizedDescription)")
               completion(false)
           } else {
               completion(true)
           }
       }
   }

   
        func fetchLeaveHistory(completion: @escaping ([LeaveHistoryItem]) -> Void) {
            guard let uid = Auth.auth().currentUser?.uid else {
                print("Failed to get user UID")
                completion([])
                return
            }

            let historyRef = Database.database().reference().child("Users").child(uid).child("leaveHistory")

            historyRef.observeSingleEvent(of: .value) { snapshot in
                var historyItems: [LeaveHistoryItem] = []

                for child in snapshot.children {
                    if let childSnapshot = child as? DataSnapshot,
                       let leaveHistoryData = childSnapshot.value as? [String:Any],
                       let leaveType = leaveHistoryData["leaveType"] as? String,
                       let note = leaveHistoryData["note"] as? String,
                       let dateInterval = leaveHistoryData["date"] as? TimeInterval {
                        
                        let date = Date(timeIntervalSince1970: dateInterval)
                        let historyItem = LeaveHistoryItem(leaveType: leaveType, note: note, date: date)
                        historyItems.append(historyItem)
                    }
                }
                completion(historyItems)
            }
        }
    func fetchLeaveBalances(completion: @escaping ([String: Int]) -> Void) {
            guard let uid = Auth.auth().currentUser?.uid else {
                print("Failed to get user UID")
                completion([:])
                return
            }

            let databaseRef = Database.database().reference().child("Users").child(uid).child("leaveCounts")
            
            databaseRef.observeSingleEvent(of: .value) { snapshot in
                guard let leaveCountsData = snapshot.value as? [String: [String: Any]] else {
                    print("Failed to fetch leave counts")
                    completion([:])
                    return
                }
                
                var leaveBalances: [String: Int] = [:]
                
                for (leaveType, leaveData) in leaveCountsData {
                    if let count = leaveData["count"] as? Int {
                        leaveBalances[leaveType] = count
                    }
                }
                
                completion(leaveBalances)
            }
        }
        
    
    func deductLeave(type: String, count: Int, completion: @escaping (Bool) -> Void) {
          guard let uid = Auth.auth().currentUser?.uid else {
              print("Failed to get user UID")
              completion(false)
              return
          }

          guard var leaveType = leaveCounts[type], let currentCount = leaveType["count"] as? Int else {
              print("Invalid leave type or leave type not found")
              completion(false)
              return
          }

          if currentCount >= count {
              leaveType["count"] = currentCount - count
              leaveCounts[type] = leaveType

              let databaseRef = Database.database().reference().child("Users").child(uid).child("leaveCounts").child(type)
              databaseRef.updateChildValues(["count": leaveType["count"]!]) { error, ref in
                  if let error = error {
                      print("Failed to update leave count: \(error.localizedDescription)")
                      completion(false)
                  } else {
                      NotificationCenter.default.post(name: UserDataManager.userDataDidChangeNotification, object: nil)
                      completion(true)
                  }
              }
          } else {
              print("Not enough leave balance")
              completion(false)
          }
      }
  
   }


   


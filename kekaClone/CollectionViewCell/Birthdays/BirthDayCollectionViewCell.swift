//
//  BirthDayCollectionViewCell.swift
//  kekaClone
//
//  Created by Divyansh on 06/05/24.
//

import UIKit

class BirthDayCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var circularView: UIView!
    
    @IBOutlet weak var viewLabel: UILabel!
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        circularView.layer.cornerRadius = circularView.frame.size.width / 2
        circularView.clipsToBounds = true
        
        
        subView.layer.cornerRadius = 8
    }
    

}

//
//  TimerCollectionViewCell.swift
//  kekaClone
//
//  Created by Divyansh on 03/05/24.
//

import UIKit
import Firebase

class TimerCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var datelabel: UILabel!
    
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var firstView: UIView!
    
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var clockInOrNot: UILabel!
    
    @IBOutlet weak var clockInLabel: UILabel!
    
    @IBOutlet weak var mainImageView: UIView!
    @IBOutlet weak var timerImageView: UIImageView!
    @IBOutlet weak var subView: UIView!
    
    var timer: Timer?
    var clockInTime: Date?
    var ref: DatabaseReference?
    
    var clockInRef: DatabaseReference?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainImageView.layer.cornerRadius = mainImageView.frame.width / 2
        mainImageView.clipsToBounds = true
        
        firstView.layer.cornerRadius = 10
        subView.layer.cornerRadius = 10
        setCurrentDateAndDay()
        ref = Database.database().reference()
        fetchClockInTime()
    }
    

    
    
    
    func formatInterval(_ interval: TimeInterval) -> String {
        let totalSeconds = Int(interval)
        
        let seconds = totalSeconds % 60
        let minutes = (totalSeconds % 3600) / 60
        let hours = seconds / 3600
        
        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    func setCurrentDateAndDay() {
        let now = Date()
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "EEEE"
        let dayString = dateFormatter.string(from: now)
        dayLabel.text = dayString
        
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        let dateString = dateFormatter.string(from: now)
        datelabel.text = dateString
    }
    
    
    
       
//        func fetchClockInTime() {
//            ref?.child("attendance").observeSingleEvent(of: .value) { snapshot , _ in
//                if let attendanceRecords = snapshot.value as? [String: [String: Any]] {
//                    for (key, recordData) in attendanceRecords {
//                        if let clockInString = recordData["clockInTime"] as? String,
//                           let clockInTime = self.dateFromString(clockInString),
//                           recordData["clockOutTime"] == nil {
//                            
//                            self.clockInTime = clockInTime
//                            self.clockInRef = self.ref?.child("attendance").child(key)
//                            
//                            let currentTime = Date()
//                            let elapsedTime = currentTime.timeIntervalSince(clockInTime)
//                            self.startTimer(with: elapsedTime)
//                            self.updateTimerLabel()
//                            
//                            let formattedElapsedTime = self.formatInterval(elapsedTime)
//                            print("Elapsed time: \(formattedElapsedTime)")
//                            
//                            let clockInString = self.formatTime(clockInTime)
//                            self.clockInLabel.text = clockInString
//                            self.clockInOrNot.text = "You are already Web clocked-in"
//                        }
//                    }
//                }
//            }
//        }
    
    

    func fetchClockInTime() {
    
        ref?.child("attendance").observeSingleEvent(of: .value) { snapshot, _ in
            if let attendanceRecords = snapshot.value as? [String: [String: Any]] {
                for (key, recordData) in attendanceRecords {
                    if let clockInString = recordData["clockInTime"] as? String,
                       let clockInTime = self.dateFromString(clockInString) {

                        self.clockInTime = clockInTime
                        self.clockInRef = self.ref?.child("attendance").child(key)

                        if let clockOutString = recordData["clockOutTime"] as? String,
                           let clockOutTime = self.dateFromString(clockOutString) {
                        
                            self.stopTimer()
                            let elapsedTime = clockOutTime.timeIntervalSince(clockInTime)
                            let formattedElapsedTime = self.formatInterval(elapsedTime)
                            self.timerLabel.text = formattedElapsedTime
                            self.clockInOrNot.text = "You have clocked-out"
                        } else {
                            
                            let currentTime = Date()
                            let elapsedTime = currentTime.timeIntervalSince(clockInTime)
                            self.startTimer(with: elapsedTime)
                            self.updateTimerLabel()
                            self.clockInOrNot.text = "You are already Web clocked-in"
                        }

                        let clockInFormatted = self.formatTime(clockInTime)
                        self.clockInLabel.text = clockInFormatted
                    }
                }
            }
        }
    }
    
       func stopTimer() {
           timer?.invalidate()
           timer = nil
       }
        
  
        func dateFromString(_ string: String) -> Date? {
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm:ss"
            return formatter.date(from: string)
        }
        
        func startTimer(with elapsedTime: TimeInterval) {
            timer?.invalidate()
            timerLabel.text = formatInterval(elapsedTime)
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimerLabel), userInfo: nil, repeats: true)
        }
        
        @objc func updateTimerLabel() {
            guard let clockInTime = clockInTime else { return }
            let currentTime = Date()
            let elapsedTime = currentTime.timeIntervalSince(clockInTime)
            timerLabel.text = formatInterval(elapsedTime)
        }
        func formatTime(_ date: Date) -> String {
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm:ss"
            return formatter.string(from: date)
        }
    }


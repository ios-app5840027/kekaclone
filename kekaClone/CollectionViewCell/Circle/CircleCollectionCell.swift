//
//  CircleCollectionCell.swift
//  kekaClone
//
//  Created by Divyansh on 03/05/24.
//

import UIKit

class CircleCollectionCell: UICollectionViewCell {

    
    @IBOutlet weak var circleImageView: UIImageView!
    
    @IBOutlet weak var circleView: UIView!
    
    @IBOutlet weak var circleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        circleView.layer.cornerRadius = circleView.frame.size.width / 2
        circleView.clipsToBounds = true
    }

}

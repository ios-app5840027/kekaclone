//
//  OffWeekCollectionViewCell.swift
//  kekaClone
//
//  Created by Divyansh on 03/05/24.
//

import UIKit

class OffWeekCollectionViewCell: UICollectionViewCell {

  
    
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var weekImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        
        mainView.layer.cornerRadius = mainView.frame.size.width / 2
        mainView.clipsToBounds = true
    }

}

//
//  InboxViewController.swift
//  kekaClone
//
//  Created by Divyansh on 06/05/24.
//

import UIKit

class InboxViewController: SideMenuHandler {
    
    @IBOutlet weak var profilePhoto: UIImageView!
    
    @IBOutlet weak var searchBar: UITextField!
    
    @IBOutlet weak var mainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profilePhoto.layer.cornerRadius = profilePhoto.frame.size.width / 2
        profilePhoto.clipsToBounds = true
        
        mainView.layer.borderWidth = 0.5
        mainView.layer.cornerRadius = 5
        mainView.layer.borderColor = UIColor.lightGray.cgColor
        
        let searchBarTappGesture = UITapGestureRecognizer(target: self, action: #selector(searchBarDidTapped))
        searchBar.isUserInteractionEnabled = true
        searchBar.addGestureRecognizer(searchBarTappGesture)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(profilePhotoTapped))
        profilePhoto.isUserInteractionEnabled = true
        profilePhoto.addGestureRecognizer(tapGestureRecognizer)
        
        fetchUserDetails()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            tabBarController?.tabBar.isHidden = true
        }
    
    @objc func profilePhotoTapped() {
        
        if isSideMenuOpen {
            closeSideMenu()
        } else {
            openSideMenu()
        }
    }
    
    
    @objc func searchBarDidTapped(){
        let pushToEmployeeDirectory = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EmployeeDirectoryVC") as! EmployeeDirectoryVC
        navigationController?.pushViewController(pushToEmployeeDirectory, animated: true)
    }
    
    func fetchUserDetails(){
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let userDataManager = appDelegate.userDataManager
            
            if let firstName = userDataManager.firstName {
                updateUI(with: userDataManager)
            } else {
                userDataManager.fetchUserData { success in
                    if success {
                        DispatchQueue.main.async {
                            self.updateUI(with: userDataManager)
                        }
                    } else {
                        print("Failed to fetch user data")
                    }
                }
            }
        }
    }
    
    func updateUI(with userDataManager: UserDataManager) {
        
            if let profileImage = userDataManager.profileImage {
                profilePhoto.image = profileImage
            }
        }
    
  
}


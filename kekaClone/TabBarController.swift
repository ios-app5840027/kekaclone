//
//  TabBarController.swift
//  kekaClone
//
//  Created by Divyansh on 02/05/24.
//

import UIKit

class TabBarController: UITabBarController {

    @IBOutlet weak var myTabBar: UITabBar!
    
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)

            guard let tabBarItems = self.tabBar.items else {
                return
            }

            for item in tabBarItems {
                item.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for:.normal)
                item.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for:.selected)
                
                tabBarController?.tabBar.isHidden = false
            }
        }
    
    
   
       
    
    override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            tabBarController?.tabBar.isHidden = true
        }
    }

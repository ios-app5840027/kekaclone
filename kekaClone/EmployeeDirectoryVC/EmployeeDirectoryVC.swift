//
//  EmployeeDirectoryVC.swift
//  kekaClone
//
//  Created by Divyansh on 12/06/24.
//

import UIKit
import Firebase

struct Employee {
    let name: String
    let photoURL: String
}
class EmployeeDirectoryVC: UIViewController {
    
    private var activityIndicator: UIActivityIndicatorView!
    var employees: [Employee] = []
    var filteredEmployees: [Employee] = []
    
    @IBOutlet weak var mySearchBar: UISearchBar!
    
    @IBOutlet weak var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchAllUsers()
        let employeeTableNib = UINib(nibName: "EmployeeTableViewCell", bundle: nil)
        myTableView.register(employeeTableNib, forCellReuseIdentifier: "EmployeeTableViewCell")
    
    }
    
    
    func fetchAllUsers(){
        showLoader()
        let databaseRef = Database.database().reference().child("Users")
        databaseRef.observeSingleEvent(of: .value) { [self] snapshot in
            guard snapshot.exists() else {
                print("Snapshot does not exist")
                return
            }
            var employees: [Employee] = []
            
            for child in snapshot.children{
                if let childSnapShot = child as? DataSnapshot,
                   let userData = childSnapShot.value as?[String: Any],
                   let firstName = userData["firstName"] as? String,
                   let lastName = userData["lastName"] as? String,
                   let profileUrl = userData["profileUrl"] as? String {
                    let employee = Employee(name: "\(firstName) \(lastName)", photoURL: profileUrl)
                    employees.append(employee)
                   
                }
            }
           
            self.employees = employees
            self.filteredEmployees = employees
            DispatchQueue.main.async{ [self] in
                self.myTableView.reloadData()
              hideLoader()
            }
        }
    }
    private func showLoader() {
        activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.center = view.center
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
    }
    
    private func hideLoader() {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
extension EmployeeDirectoryVC : UITableViewDataSource , UITableViewDelegate , UISearchBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredEmployees.count
 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTableView.dequeueReusableCell(withIdentifier: "EmployeeTableViewCell", for: indexPath) as! EmployeeTableViewCell
        
        let employee = filteredEmployees[indexPath.row]
        
        cell.userNameLabel.text = employee.name
    
        if let url = URL(string: employee.photoURL) {
            DispatchQueue.global().async {
                if let data = try? Data(contentsOf: url) {
                    DispatchQueue.main.async {
                        cell.myImageView.image = UIImage(data: data)
                    }
                }
            }
        }
        
        return cell
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            filteredEmployees = employees
        } else {
            filteredEmployees = employees.filter { $0.name.lowercased().contains(searchText.lowercased()) }
        }
        myTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
}

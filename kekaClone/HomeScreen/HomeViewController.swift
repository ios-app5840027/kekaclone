//
//  HomeViewController.swift
//  kekaClone
//
//  Created by Divyansh on 02/05/24.
//

import UIKit

class HomeViewController: SideMenuHandler {
    
    @IBOutlet weak var searchBar: UITextField!
    
    @IBOutlet weak var profilePhoto: UIImageView!
    
    @IBOutlet weak var myTableView: UITableView!
    var sideMenuHandler: SideMenuHandler?
    
    var cirleData: [(String, String)] = [
            ("airplane.departure", NSLocalizedString("apply_leave", comment: "")),
            ("house.lodge", NSLocalizedString("apply_wfh", comment: "")),
            ("book", NSLocalizedString("view_payslip", comment: "")),
            ("menucard", NSLocalizedString("raise_expense", comment: "")),
            ("list.bullet.rectangle", NSLocalizedString("leave_balance", comment: ""))
        ]
    
    var annivesaryData = [(UIColor.systemMint.withAlphaComponent(0.3),"HK","Harsh kundra"),
                          (UIColor.systemTeal.withAlphaComponent(0.3),"GK","Gagandeep "),
                          (UIColor.secondaryLabel.withAlphaComponent(0.3),"DJ","Divyansh Jaryal"),
                          (UIColor.red.withAlphaComponent(0.3),"HK","Harsh kundra"),
                          (UIColor.blue.withAlphaComponent(0.3),"GK","Gagandeep ")]
    override func viewDidLoad() {
        super.viewDidLoad()
        profilePhoto.layer.cornerRadius = profilePhoto.frame.size.width / 2
        profilePhoto.clipsToBounds = true
        
        
        let searchBarTappGesture = UITapGestureRecognizer(target: self, action: #selector(searchBarDidTapped))
        searchBar.isUserInteractionEnabled = true
        searchBar.addGestureRecognizer(searchBarTappGesture)
        
        let mainTableaCell = UINib(nibName: "MainTableViewCell", bundle: nil)
        myTableView.register(mainTableaCell, forCellReuseIdentifier: "MainTableViewCell")
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(profilePhotoTapped))
        profilePhoto.isUserInteractionEnabled = true
        profilePhoto.addGestureRecognizer(tapGestureRecognizer)
        
        fetchUserDetails()
       
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        myTableView.reloadData()
        tabBarController?.tabBar.isHidden = false
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            tabBarController?.tabBar.isHidden = true
        }
    
    @objc func searchBarDidTapped(){
        let pushToEmployeeDirectory = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EmployeeDirectoryVC") as! EmployeeDirectoryVC
        navigationController?.pushViewController(pushToEmployeeDirectory, animated: true)
    }
    
    func fetchUserDetails(){
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let userDataManager = appDelegate.userDataManager
            
            if let firstName = userDataManager.firstName {
                updateUI(with: userDataManager)
            } else {
                userDataManager.fetchUserData { success in
                    if success {
                        DispatchQueue.main.async {
                            self.updateUI(with: userDataManager)
                        }
                    } else {
                        print("Failed to fetch user data")
                    }
                }
            }
        }
    }
   
    @objc func profilePhotoTapped() {
        
        if isSideMenuOpen  {
            closeSideMenu()
        } else {
            openSideMenu()
           
        }
    }
    
    func updateUI(with userDataManager: UserDataManager) {
        
            if let profileImage = userDataManager.profileImage {
                profilePhoto.image = profileImage
            }
        }
    
    
}

extension HomeViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = myTableView.dequeueReusableCell(withIdentifier: "MainTableViewCell", for: indexPath) as! MainTableViewCell
        cell.tag = indexPath.row
        cell.indexPath = indexPath
        switch indexPath.row{
        case 0 :
            cell.currentCollectionView = .circleCollectionCell
            cell.circleData = self.cirleData
            cell.parentVC = self
            cell.myCollectionView.reloadData()
        case 1:
            cell.currentCollectionView = .timerCollectionCell
            cell.parentVC = self
            cell.myCollectionView.reloadData()
            
        case 2:
            cell.currentCollectionView = .offThisWeek
            cell.myCollectionView.reloadData()
        case 3:
            cell.currentCollectionView = .birthDay
            cell.annivesaryData = self.annivesaryData
            cell.myCollectionView.reloadData()
        case 4:
            cell.currentCollectionView = .announcements
            cell.myCollectionView.reloadData()
        default:
            cell.currentCollectionView = .holidays
            cell.myCollectionView.reloadData()
        }
        return cell
    
        }
       

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row{
        case 0:
            return 100
        case 1:
            return 200
        case 2:
            return 160
        case 3:
            return 170
        case 4:
            return 90
        default:
            return 160
        }
    }
    func reloadTimer() {
        if let cell = myTableView.cellForRow(at: IndexPath(item: 1, section: 0)) as? MainTableViewCell {
            cell.reloadTimer()
        }
    }
  
      
}


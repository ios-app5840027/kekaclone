//
//  MainTableViewCell.swift
//  kekaClone
//
//  Created by Divyansh on 02/05/24.
//

import UIKit
enum MultipleCollectionViews : Int {
    case circleCollectionCell = 0
    case timerCollectionCell
    case offThisWeek
    case birthDay
    case announcements
    case holidays
}

class MainTableViewCell: UITableViewCell {
    var dataArr = [String]()
   
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    @IBOutlet weak var headingLabel: UILabel!
    
    @IBOutlet weak var headingLabelHight: NSLayoutConstraint!
    
    weak var parentVC : UIViewController?
    var indexPath: IndexPath?
    var currentCollectionView: MultipleCollectionViews?
    
    var circleData = [(String,String)]()
    var annivesaryData = [(UIColor,String,String)]()

    override func awakeFromNib() {
        super.awakeFromNib()
        registerCollectionViewCells()
        self.myCollectionView.delegate = self
        self.myCollectionView.dataSource = self
        
    
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
      
        
    }
    
    
    func registerCollectionViewCells() {
           let circleCollectionCellNib = UINib(nibName: "CircleCollectionCell", bundle: nil)
           myCollectionView.register(circleCollectionCellNib, forCellWithReuseIdentifier: "CircleCollectionCell")
           
           let timerCollectionCellNib = UINib(nibName: "TimerCollectionViewCell", bundle: nil)
           myCollectionView.register(timerCollectionCellNib, forCellWithReuseIdentifier: "TimerCollectionViewCell")
        
        
        let offThisWeekCollectionCellNib = UINib(nibName: "OffWeekCollectionViewCell", bundle: nil)
        myCollectionView.register(offThisWeekCollectionCellNib, forCellWithReuseIdentifier: "OffWeekCollectionViewCell")
        
        let birthdayCollectionViewCellNib = UINib(nibName: "BirthDayCollectionViewCell", bundle: nil)
        myCollectionView.register(birthdayCollectionViewCellNib, forCellWithReuseIdentifier: "BirthDayCollectionViewCell")
        
        let announcementsCollectionNib = UINib(nibName: "AnnouncementCollectionVC", bundle: nil)
        myCollectionView.register(announcementsCollectionNib, forCellWithReuseIdentifier: "AnnouncementCollectionVC")
        
        let holidaysCollectionNib = UINib(nibName: "HolidaysCollectionVC", bundle: nil)
        myCollectionView.register(holidaysCollectionNib, forCellWithReuseIdentifier: "HolidaysCollectionVC")
       }
}




extension MainTableViewCell: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch currentCollectionView {
        case .circleCollectionCell:
            return circleData.count
        case .timerCollectionCell:
            return 1
        case .offThisWeek:
            return 1
        case.birthDay:
            return annivesaryData.count
        case .announcements:
            return 1
        case .holidays:
            return 4
        case .none:
            return 0
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        updateHeadingLabel()
        switch currentCollectionView {
            
                case .circleCollectionCell:
            
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CircleCollectionCell", for: indexPath) as! CircleCollectionCell
                    cell.circleImageView.image = UIImage(systemName: circleData[indexPath.row].0)
                    cell.circleLabel.text = circleData[indexPath.row].1
                    return cell
       
                case .timerCollectionCell:
           
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimerCollectionViewCell", for: indexPath) as! TimerCollectionViewCell
                    cell.fetchClockInTime()
                    return cell
            
                case .offThisWeek:
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OffWeekCollectionViewCell", for: indexPath) as! OffWeekCollectionViewCell
                    return cell
            
               case .birthDay:
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BirthDayCollectionViewCell", for: indexPath) as! BirthDayCollectionViewCell
            cell.circularView.backgroundColor = annivesaryData[indexPath.row].0
            cell.viewLabel.text = annivesaryData[indexPath.row].1
            cell.nameLabel.text = annivesaryData[indexPath.row].2
                 return cell
            
        case .announcements:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnouncementCollectionVC", for: indexPath) as! AnnouncementCollectionVC
            return cell
            
        case .holidays:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HolidaysCollectionVC", for: indexPath) as! HolidaysCollectionVC
            return cell
        case .none:
            return UICollectionViewCell()
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch currentCollectionView{
        case .circleCollectionCell :
            let width = myCollectionView.frame.width / 5 - 10
            return CGSize(width: width, height: 95)
        case .timerCollectionCell:
            let width = myCollectionView.frame.width / 1
            return CGSize(width: width, height: 170)
            
        case .offThisWeek:
            let width = collectionView.frame.width / 1
            return CGSize(width: width, height: 130)
            
        case .birthDay:
            let width = collectionView.frame.width / 5 - 10
            
            return CGSize(width: width, height: 110)
        case .announcements:
            let width = collectionView.frame.width / 1 - 10
            return CGSize(width: width, height: 80)
        case .holidays:
            let width = collectionView.frame.width / 1 - 10
            return CGSize(width: width, height: 120)
        case .none:
            return CGSize(width: 0, height: 0)
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let parentVC = parentVC as? HomeViewController else {
               return
           }
           
           
      
           guard let mainCell = collectionView.superview?.superview as? MainTableViewCell,
                 let mainCellIndexPath = mainCell.indexPath else {
               return
           }

         
           if mainCell.tag == mainCellIndexPath.row {
               if mainCell.tag == 0 && indexPath.item == 0 {
                
                   if let applyLeaveVC = parentVC.storyboard?.instantiateViewController(withIdentifier: "ApplyLeaveVC") {
                       parentVC.navigationController?.pushViewController(applyLeaveVC, animated: true)
                   }
               
               }
               else if mainCell.tag == 0 && indexPath.item == 4{
                   if let leaveBalance = parentVC.storyboard?.instantiateViewController(withIdentifier: "LeaveBalanceVC"){
                       parentVC.navigationController?.pushViewController(leaveBalance, animated: true)
                   }
               }
               else if mainCell.tag == 1 && indexPath.item == 0 {
           
                   if let attendanceVC = parentVC.storyboard?.instantiateViewController(withIdentifier: "AttendanceViewController") as? AttendanceViewController {
                       parentVC.navigationController?.pushViewController(attendanceVC, animated: true)
                   }
               }
           }
       }
    
    func updateHeadingLabel() {
        switch currentCollectionView {
        case .circleCollectionCell, .timerCollectionCell:
            headingLabel.isHidden = true
            headingLabelHight.constant = 0
        case .offThisWeek:
            headingLabel.isHidden = false
            headingLabelHight.constant = 20
            headingLabel.text = NSLocalizedString("off_this_week_heading", comment: "")
        case .birthDay:
            headingLabel.isHidden = false
            headingLabelHight.constant = 20
            headingLabel.text = NSLocalizedString("birthday_heading", comment: "")
        case .announcements:
            headingLabel.isHidden = false
            headingLabelHight.constant = 20
            headingLabel.text = NSLocalizedString("announcements_heading", comment: "")
        case .holidays:
            headingLabel.isHidden = false
            headingLabelHight.constant = 20
            headingLabel.text = NSLocalizedString("holidays_heading", comment: "")
        case .none:
            headingLabel.isHidden = true
            headingLabelHight.constant = 0
        }
    }
    
    func reloadTimer() {
        if let cell = myCollectionView.cellForItem(at: IndexPath(item: 1, section: 0)) as? TimerCollectionViewCell {
            cell.fetchClockInTime()
        }
    }
    
    
}

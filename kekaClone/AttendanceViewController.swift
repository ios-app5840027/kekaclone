//
//  AttendanceViewController.swift
//  kekaClone
//
//  Created by Divyansh on 22/05/24.
//

import UIKit
import FirebaseDatabase
import UserNotifications

class AttendanceViewController: UIViewController {
    
    @IBOutlet weak var clockInButton: UIButton!
    
    @IBOutlet weak var clockOutButton: UIButton!
    
    @IBOutlet weak var currentDay: UILabel!
    @IBOutlet weak var currentDate: UILabel!
    
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var wantToClockIn: UILabel!
    
    @IBOutlet weak var timerView: UIView!
    @IBOutlet weak var clockInLabel: UILabel!
    
    @IBOutlet weak var clockOutLabel: UILabel!
    
    
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var clockInlabel2: UILabel!
    
    @IBOutlet weak var clockInLabel3: UILabel!
    
    @IBOutlet weak var clockOutLabel2: UILabel!
    
    
    var timer: Timer?
    var clockInTime: Date?
    var ref: DatabaseReference!
    var clockInRef: DatabaseReference?
    var attendanceData: [String: Any] = [:]
    let center =  UNUserNotificationCenter.current()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timerView.layer.cornerRadius = 8
        self.title = "Attendance - Today"
        timerLabel.text = "00:00:00"
        clockOutButton.isEnabled = false
        setCurrentDateAndDay()
      
        ref = Database.database().reference()
        fetchClockInTime()
    }
    func setCurrentDateAndDay() {
        let now = Date()
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "EEEE"
        let dayString = dateFormatter.string(from: now)
        currentDay.text = dayString
        
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        let dateString = dateFormatter.string(from: now)
        currentDate.text = dateString
    }
    
    @IBAction func clockIn(_ sender: UIButton) {
        clockInTime = Date()
        let clockInString = formatTime(clockInTime!)
        
        clockInLabel.text = clockInString
        clockInlabel2.text = clockInString
        clockInLabel3.text = clockInString
        clockInButton.isEnabled = false
        clockOutButton.isEnabled = true
        wantToClockIn.text = "You are already Web clocked-in"
        
        startTimer()
        
        if let clockInTime = clockInTime {
            attendanceData = [
                "clockInTime": clockInString,
                "date": currentDate.text ?? "",
                "day": currentDay.text ?? ""
            ]
            ref.child("attendance").childByAutoId().setValue(attendanceData)
        }
       
    
    let content = UNMutableNotificationContent()
       content.title = "Clock In"
       content.body = "You have logged in at \(clockInString)"
       content.sound = UNNotificationSound.default
       
     
   
        if let image = UIImage(named: "clockin") {
              
              if let imageData = image.pngData() {
                  let tempDirectory = FileManager.default.temporaryDirectory
                  let tempFileURL = tempDirectory.appendingPathComponent("clockin.png")
                  do {
                      try imageData.write(to: tempFileURL)
                      
                    
                      let attachment = try UNNotificationAttachment(identifier: "image", url: tempFileURL, options: nil)
                      content.attachments = [attachment]
                  } catch {
                      print("Error creating notification attachment: \(error.localizedDescription)")
                  }
              }
          } else {
              print("Resource 'clockin' not found in Assets catalog.")
          }
       let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
       let request = UNNotificationRequest(identifier: "clockInNotification", content: content, trigger: trigger)
       
       center.add(request) { error in
           if let error = error {
               print("Error scheduling notification: \(error)")
           }
       }
   }

    func fetchClockInTime() {
        ref.child("attendance").observeSingleEvent(of: .value) { snapshot in
            if let attendanceRecords = snapshot.value as? [String: [String: Any]] {
                for (key, recordData) in attendanceRecords {
                    if let clockInString = recordData["clockInTime"] as? String,
                       let clockInTime = self.dateFromString(clockInString),
                       recordData["clockOutTime"] == nil {
                        
                        self.clockInTime = clockInTime
                        self.clockInRef = self.ref.child("attendance").child(key)
                        
                        let currentTime = Date()
                        let elapsedTime = currentTime.timeIntervalSince(clockInTime)
                        self.startTimer(with: elapsedTime)
                        self.updateTimerLabel()
                        
                        let formattedElapsedTime = self.formatInterval(elapsedTime)
                       
                        
                        let clockInString = self.formatTime(clockInTime)
                        self.clockInLabel.text = clockInString
                        self.clockInlabel2.text = clockInString
                        self.clockInLabel3.text = clockInString
                        self.clockInButton.isEnabled = false
                        self.clockOutButton.isEnabled = true
                        self.wantToClockIn.text = "You are already Web clocked-in"
                    }
                }
            }
        }
    }
    
    
    @IBAction func clockOut(_ sender: UIButton) {
        showAlert()
    }
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
     
        navigationController?.popViewController(animated: true)
    }
    
    
    func clockOutHandler(action: UIAlertAction) {
        guard let clockInTime = clockInTime else { return }
        
        timer?.invalidate()
        timer = nil
  
        let clockOutTime = Date()
        let clockOutString = formatTime(clockOutTime)
   
        clockOutLabel.text = clockOutString
        clockOutLabel2.text = clockOutString
        clockOutButton.isEnabled = false
        wantToClockIn.isHidden = true
        
        let workedTime = clockOutTime.timeIntervalSince(clockInTime)
        timerLabel.text = formatInterval(workedTime)
        
        let clockOutData: [String: Any] = [
            "clockOutTime": clockOutString,
            "workedTime": formatInterval(workedTime)
        ]
        clockInRef?.updateChildValues(clockOutData)
        attendanceData.merge(clockOutData) { (_, new) in new }
      
    }
    
    
  
    func startTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimerLabel), userInfo: nil, repeats: true)
        updateTimerLabel()
    }
    
    @objc func updateTimerLabel() {
        guard let clockInTime = clockInTime else { return }
        let currentTime = Date()
        let elapsedTime = currentTime.timeIntervalSince(clockInTime)
        timerLabel.text = formatInterval(elapsedTime)
    }
    
    func formatTime(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm:ss"
        return formatter.string(from: date)
    }
    
    func formatInterval(_ interval: TimeInterval) -> String {
        let totalSeconds = Int(interval)
        
        let seconds = totalSeconds % 60
        let minutes = (totalSeconds % 3600) / 60
        let hours = seconds / 3600
        
        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Confirm", message: "Do you want to clock out?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: clockOutHandler))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func dateFromString(_ string: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm:ss"
        return formatter.date(from: string)
    }
    
    func startTimer(with elapsedTime: TimeInterval) {
        timer?.invalidate()
        timerLabel.text = formatInterval(elapsedTime)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimerLabel), userInfo: nil, repeats: true)
    }
    
    
    
    
    
    
    
}



//
//  LanguagesVC.swift
//  kekaClone
//
//  Created by Divyansh on 18/06/24.
//

import UIKit

class LanguagesVC: UIViewController {

    
    var languages = ["English","Hindi"]
    var languagesData = ["English", "हिंदी"]
    var selectedLanguageIndex: Int = 0
    
    @IBOutlet weak var myTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let languageCellNib = UINib(nibName: "LanguagesTableCell", bundle: nil)
        myTableView.register(languageCellNib, forCellReuseIdentifier: "LanguagesTableCell")
        if let savedLanguageIndex = UserDefaults.standard.value(forKey: "selectedLanguageIndex") as? Int {
                   selectedLanguageIndex = savedLanguageIndex
               }

               myTableView.reloadData()
          }
        
    

    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
    }
}

extension LanguagesVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTableView.dequeueReusableCell(withIdentifier: "LanguagesTableCell", for: indexPath) as! LanguagesTableCell
        
        
        cell.mainLabel.text = languagesData[indexPath.row]
        cell.subLabel.text = languages[indexPath.row]
        
        if indexPath.row == selectedLanguageIndex {
            cell.checkMark.image = UIImage(systemName:"checkmark.seal.fill")
        } else {
            cell.checkMark.image = nil
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedLanguageIndex = indexPath.row
        UserDefaults.standard.set(selectedLanguageIndex, forKey: "selectedLanguageIndex")
        tableView.reloadData()
        applyLanguage(languages[indexPath.row])
        
       
    }
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    
    func applyLanguage(_ language: String) {
        var languageCode: String = "en"
        
      
        switch language {
        case "English":
            languageCode = "en"
        case "Hindi":
            languageCode = "hi"
        default:
            break
        }
        
       
        UserDefaults.standard.set([languageCode], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        
        Bundle.setLanguage(languageCode)
        resetRootViewController()
       
        }
    
    func resetRootViewController() {
           guard let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else {
               return
           }
           sceneDelegate.resetRootViewController()
       }
    }



extension Bundle {
    internal static var bundleKey: UInt8 = 0

    static func setLanguage(_ language: String) {
        defer {
            object_setClass(Bundle.main, PrivateBundle.self)
        }

        objc_setAssociatedObject(Bundle.main, &bundleKey, Bundle(path: Bundle.main.path(forResource: language, ofType: "lproj")!), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
}

private class PrivateBundle: Bundle {
    override func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        let bundle = objc_getAssociatedObject(self, &Bundle.bundleKey) as? Bundle
        return bundle?.localizedString(forKey: key, value: value, table: tableName) ?? super.localizedString(forKey: key, value: value, table: tableName)
    }
}

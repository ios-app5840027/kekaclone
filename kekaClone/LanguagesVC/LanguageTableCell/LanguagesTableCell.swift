//
//  LanguagesTableCell.swift
//  kekaClone
//
//  Created by Divyansh on 18/06/24.
//

import UIKit

class LanguagesTableCell: UITableViewCell {

    @IBOutlet weak var mainLabel: UILabel!
    
    @IBOutlet weak var checkMark: UIImageView!
    @IBOutlet weak var subLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

     
    }
    
}

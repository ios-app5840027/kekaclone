//
//  AttendanceCollectionViewCell.swift
//  kekaClone
//
//  Created by Divyansh on 06/05/24.
//

import UIKit

class AttendanceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var iconImage: UIImageView!
    
    @IBOutlet weak var mainLabel: UILabel!
    
    @IBOutlet weak var subLabel: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mainView.layer.borderWidth = 0.5
        mainView.layer.cornerRadius = 5
        mainView.layer.borderColor = UIColor.lightGray.cgColor
    }

}

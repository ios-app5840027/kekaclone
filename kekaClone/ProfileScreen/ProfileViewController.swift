//
//  ProfileViewController.swift
//  kekaClone
//
//  Created by Divyansh on 02/05/24.
//

import UIKit

class ProfileViewController: SideMenuHandler {

    @IBOutlet weak var salaryPayView: UIView!
    
    @IBOutlet weak var mainFinanceView: UIView!
    
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var expensesSubLabel: UILabel!
    
    @IBOutlet weak var PastClaimsSubLabel: UILabel!
    @IBOutlet weak var myPaySubLabel: UILabel!
    @IBOutlet weak var salaryTaxView: UIView!
    
    @IBOutlet weak var manageSubLabel: UILabel!
    @IBOutlet weak var pendindSubLabel: UILabel!
    @IBOutlet weak var expensesClaimView: UIView!
    
    @IBOutlet weak var expensesPendingView: UIView!
    
    @IBOutlet weak var expensesPastClaimView: UIView!
    
    @IBOutlet weak var profilePhoto: UIImageView!
    
    @IBOutlet weak var myTableView: UITableView!
    
    @IBOutlet weak var timeButton: UIButton!
    
    @IBOutlet weak var financesButton: UIButton!
    
    @IBOutlet weak var timeButtonAction: UIView!
    
    @IBOutlet weak var financesButtonAction: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        expensesSubLabel.text = NSLocalizedString("expensesSubLabel", comment: "")
        PastClaimsSubLabel.text = NSLocalizedString("PastClaimsSubLabel", comment: "")
        pendindSubLabel.text = NSLocalizedString("pendindSubLabel", comment: "")
        myPaySubLabel.text = NSLocalizedString("myPaySubLabel", comment: "")
        manageSubLabel.text = NSLocalizedString("manageSubLabel", comment: "")
        
        profilePhoto.layer.cornerRadius = profilePhoto.frame.size.width / 2
        profilePhoto.clipsToBounds = true
        financesButtonAction.isHidden = true
        
        myTableView.isHidden = false
        mainFinanceView.isHidden = true
        cornerRadiusBorderToView()
        
        let ProfileTableCellNib = UINib(nibName: "ProfileTableViewCell", bundle: nil)
        myTableView.register(ProfileTableCellNib, forCellReuseIdentifier: "ProfileTableViewCell")
        
        
        let searchBarTappGesture = UITapGestureRecognizer(target: self, action: #selector(searchBarDidTapped))
        searchBar.isUserInteractionEnabled = true
        searchBar.addGestureRecognizer(searchBarTappGesture)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(profilePhotoTapped))
        profilePhoto.isUserInteractionEnabled = true
        profilePhoto.addGestureRecognizer(tapGestureRecognizer)
        fetchUserDetails()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            tabBarController?.tabBar.isHidden = true
        }
    
    
    func fetchUserDetails(){
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let userDataManager = appDelegate.userDataManager
            
            if let firstName = userDataManager.firstName {
                updateUI(with: userDataManager)
            } else {
                userDataManager.fetchUserData { success in
                    if success {
                        DispatchQueue.main.async {
                            self.updateUI(with: userDataManager)
                        }
                    } else {
                        print("Failed to fetch user data")
                    }
                }
            }
        }
    }
    
    @objc func searchBarDidTapped(){
        let pushToEmployeeDirectory = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EmployeeDirectoryVC") as! EmployeeDirectoryVC
        navigationController?.pushViewController(pushToEmployeeDirectory, animated: true)
    }
    
    func updateUI(with userDataManager: UserDataManager) {
        
            if let profileImage = userDataManager.profileImage {
                profilePhoto.image = profileImage
            }
        }
    
    @objc func profilePhotoTapped() {
        
        if isSideMenuOpen  {
            closeSideMenu()
        } else {
            openSideMenu()
        }
    }
    
    @IBAction func timeButtonDidTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
               self.mainFinanceView.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
               self.mainFinanceView.alpha = 0
               self.myTableView.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
               self.myTableView.alpha = 0
           }) { _ in
               self.timeButtonAction.isHidden = false
               self.financesButtonAction.isHidden = true
               self.view.viewWithTag(100)?.removeFromSuperview()
               self.timeButton.titleLabel?.tintColor = .purple.withAlphaComponent(0.5)
               self.financesButton.titleLabel?.tintColor = .lightGray
               self.mainFinanceView.isHidden = true
               self.myTableView.isHidden = false

         
               self.mainFinanceView.transform = .identity
               self.myTableView.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)

               UIView.animate(withDuration: 0.3) {
                   self.myTableView.transform = .identity
                   self.myTableView.alpha = 1
               }
           }
    }
    
    
    @IBAction func financeButton(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3, animations: {
                self.myTableView.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
                self.myTableView.alpha = 0
                self.mainFinanceView.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
                self.mainFinanceView.alpha = 0
            }) { _ in
                self.financesButtonAction.isHidden = false
                self.timeButtonAction.isHidden = true
                self.financesButton.titleLabel?.tintColor = .purple.withAlphaComponent(0.5)
                self.timeButton.titleLabel?.tintColor = .lightGray
                self.mainFinanceView.isHidden = false
                self.myTableView.isHidden = true

               
                self.myTableView.transform = .identity
                self.mainFinanceView.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)

                UIView.animate(withDuration: 0.3) {
                    self.mainFinanceView.transform = .identity
                    self.mainFinanceView.alpha = 1
                }
            }
    }
    
    func cornerRadiusBorderToView (){
        
        let views = [salaryPayView, salaryTaxView, expensesClaimView, expensesPendingView, expensesPastClaimView]
            
            for view in views {
                view?.layer.cornerRadius = 6
                view?.layer.borderWidth = 0.3
                view?.layer.borderColor = UIColor.lightGray.cgColor
                view?.clipsToBounds = true
            }
    }
    
    }
    
    
extension ProfileViewController: UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
        cell.tag = indexPath.row
        cell.indexPath = indexPath
        switch indexPath.row{
        case 0:
            cell.contentType = .Attendance
            cell.parentVC = self
        case 1:
            cell.contentType = .leave
            cell.parentVC = self
        default:
            cell.contentType = .holidays
                    }       
            
            return cell
        }
            
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            switch indexPath.row{
            case 0:
                return 300
            case 1:
                return 200
            default:
                return 160
            }
        }
        
    }


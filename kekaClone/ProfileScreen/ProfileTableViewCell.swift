//
//  ProfileTableViewCell.swift
//  kekaClone
//
//  Created by Divyansh on 06/05/24.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    enum ProfileCollectionView{
        case Attendance
        case leave
        case holidays
      
        
    }
    @IBOutlet weak var headingLable: UILabel!
    
    @IBOutlet weak var heightOfLabel: NSLayoutConstraint!
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    var indexPath: IndexPath?
    weak var parentVC : UIViewController?
    var contentType : ProfileCollectionView = .Attendance
    var attendanceData = [
            ("pin", NSLocalizedString("clock_in", comment: ""), NSLocalizedString("clock_in_description", comment: "")),
            ("doc.richtext.he", NSLocalizedString("raise_request", comment: ""), NSLocalizedString("raise_request_description", comment: "")),
            ("watch.analog", NSLocalizedString("logs_and_shifts", comment: ""), NSLocalizedString("logs_and_shifts_description", comment: "")),
            ("note", NSLocalizedString("request_history", comment: ""), NSLocalizedString("request_history_description", comment: ""))
        ]
        
        var leaveData = [
            ("airplane", NSLocalizedString("apply_leave", comment: ""), NSLocalizedString("apply_leave_description", comment: "")),
            ("arrow.uturn.left.circle", NSLocalizedString("leave_balance", comment: ""), NSLocalizedString("leave_balance_description", comment: ""))
        ]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.myCollectionView.dataSource = self
        self.myCollectionView.delegate = self
        
        registerNibs()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
    func registerNibs (){
        let attendanceNib = UINib(nibName: "AttendanceCollectionViewCell", bundle: nil)
        myCollectionView.register(attendanceNib, forCellWithReuseIdentifier: "AttendanceCollectionViewCell")
        
        let holidaysCollectionNib = UINib(nibName: "HolidaysCollectionVC", bundle: nil)
        myCollectionView.register(holidaysCollectionNib, forCellWithReuseIdentifier: "HolidaysCollectionVC")
    }
    
}
extension ProfileTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch contentType{
        case .Attendance:
            return 4
            
      
            
        case .leave:
            return 2
        case .holidays:
            return 4
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch contentType{
        case .Attendance:
            headingLable.text = NSLocalizedString("attendance", comment: "")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttendanceCollectionViewCell", for: indexPath) as! AttendanceCollectionViewCell
            cell.iconImage.image = UIImage(systemName: attendanceData[indexPath.row].0)
            cell.mainLabel.text = attendanceData[indexPath.row].1
            cell.subLabel.text = attendanceData[indexPath.row].2
            return cell
        case .leave:
            headingLable.text = NSLocalizedString("leaves", comment: "")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttendanceCollectionViewCell", for: indexPath) as! AttendanceCollectionViewCell
           
            cell.iconImage.image = UIImage(systemName: leaveData[indexPath.row].0)
            cell.mainLabel.text = leaveData[indexPath.row].1
            cell.subLabel.text = leaveData[indexPath.row].2
            return cell
            
        case .holidays:
            headingLable.text = NSLocalizedString("upcoming_holidays", comment: "")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HolidaysCollectionVC", for: indexPath) as! HolidaysCollectionVC
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                           layout.scrollDirection = .horizontal
                       }
            
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch contentType{
        case .Attendance :
            let width = myCollectionView.frame.width / 2 - 10
            
            return CGSize(width: width, height: 120)
        case .leave:
            let width = myCollectionView.frame.width / 2 - 10
            return CGSize(width: width, height: 120)
        case .holidays:
            let width = collectionView.frame.width / 1 - 15
            return CGSize(width: width, height: 120)
            
           
            }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let parentVC = parentVC as? ProfileViewController else {
               return
           }
           
           
      
           guard let mainCell = collectionView.superview?.superview as? ProfileTableViewCell,
                 let mainCellIndexPath = mainCell.indexPath else {
               return
           }

         
           if mainCell.tag == mainCellIndexPath.row {
               if mainCell.tag == 0 && indexPath.item == 0 {
                
                   if let ClockIN = parentVC.storyboard?.instantiateViewController(withIdentifier: "AttendanceViewController") {
                       parentVC.navigationController?.pushViewController(ClockIN, animated: true)
                   }
               
               }
               else if mainCell.tag == 1 && indexPath.item == 0{
                   if let leaveBalance = parentVC.storyboard?.instantiateViewController(withIdentifier: "ApplyLeaveVC"){
                       parentVC.navigationController?.pushViewController(leaveBalance, animated: true)
                   }
               }
               else if mainCell.tag == 1 && indexPath.item == 1 {
           
                   if let attendanceVC = parentVC.storyboard?.instantiateViewController(withIdentifier: "LeaveBalanceVC") as? LeaveBalanceVC {
                       parentVC.navigationController?.pushViewController(attendanceVC, animated: true)
                   }
               }
           }
       }

}
